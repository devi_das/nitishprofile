import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AboutComponent} from './about/about.component';
import { ExperienceComponent } from 'src/app/experience/experience.component';
import { EducationComponent } from 'src/app/education/education.component';
import { InterestComponent } from 'src/app/interest/interest.component';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
const routes: Routes = [
  {
    path:"about",
    component:AboutComponent
    
  },
  {
    path:"experince",
    component:ExperienceComponent
  },
  {
    path:"education",
    component:EducationComponent
  },
  {
    path:"interest",
    component:InterestComponent
  },
  {
    path:"contact",
    component:ContactsComponent

  },
  {
    path:'',
    redirectTo:'about',
    pathMatch:"full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
